from flask import Flask, request, send_from_directory, render_template
from flask_cors import CORS
from datetime import datetime
import urllib
import json
app = Flask(__name__, static_url_path='')
CORS(app)

@app.route('/', methods=['GET', 'POST'])
def homepage():
    if request.method == "GET":
        return render_template('index.html')

    elif request.method == "POST":
        # get form data on POST
        app = request.form['app-1']
        user = request.form['user']
        rating = request.form['rating']

        # populate data from form
        data =  {
            "Inputs": {
                "input1":{
                    "ColumnNames": ["user_ID", "app_ID", "Expr1002"],
                    "Values": [ [ user, app, rating ], ]
                },
            },
            "GlobalParameters": {}
        }

        body = str.encode(json.dumps(data))
        url = 'https://ussouthcentral.services.azureml.net/workspaces/e8cbeda46238441dbbef537b1a4a45c6/services/67e6bdfe74ad468bb30a2a517030c237/execute?api-version=2.0&details=true'
        api_key = 'g7YWWwtSZCPYtmya7JUYlHIaaxulGk2KeE5JdgHM2kJMeXKO83VFmfvUGUMj7wKbV2CMCUJry/sydK8nj1JH+A=='
        headers = {'Content-Type':'application/json', 'Authorization':('Bearer '+ api_key), 'Access-Control-Allow-Origin':'*'}

        # make request to Azure API using form data
        req = urllib.request.Request(url, body, headers)

        try:
            response = urllib.request.urlopen(req)
            result = response.read()
            jsonResult = json.loads(result.decode('utf-8'))
            # parse json for app list
            apps = jsonResult['Results']['output1']['value']['Values'][0]
            # pass result to result template
            return render_template('result.html', apps=apps)

        except urllib.error.HTTPError as error:
            print("The request failed with status code: " + str(error.code))
            print(error.info())
            print(json.loads(error.read()))
            return error.info()

@app.route('/tableau')
def tableau():
    return render_template('tableau.html')

@app.route('/result')
def result():
    if request.method == "GET":
        return render_template('result.html')

    elif request.method == "POST":
        # get form data on POST
        app = request.form['app-1']
        user = request.form['user']
        rating = request.form['rating']

        # populate data from form
        data =  {
            "Inputs": {
                "input1":{
                    "ColumnNames": ["user_ID", "app_ID", "Expr1002"],
                    "Values": [ [ user, app, rating ], ]
                },
            },
            "GlobalParameters": {}
        }

        body = str.encode(json.dumps(data))
        url = 'https://ussouthcentral.services.azureml.net/workspaces/e8cbeda46238441dbbef537b1a4a45c6/services/67e6bdfe74ad468bb30a2a517030c237/execute?api-version=2.0&details=true'
        api_key = 'g7YWWwtSZCPYtmya7JUYlHIaaxulGk2KeE5JdgHM2kJMeXKO83VFmfvUGUMj7wKbV2CMCUJry/sydK8nj1JH+A=='
        headers = {'Content-Type':'application/json', 'Authorization':('Bearer '+ api_key), 'Access-Control-Allow-Origin':'*'}

        # make request to Azure API using form data
        req = urllib.request.Request(url, body, headers)

        try:
            response = urllib.request.urlopen(req)
            result = response.read()
            jsonResult = json.loads(result.decode('utf-8'))
            # parse json for app list
            apps = jsonResult['Results']['output1']['value']['Values'][0]
            # pass result to result template
            return render_template('result.html', apps=apps)

        except urllib.error.HTTPError as error:
            print("The request failed with status code: " + str(error.code))
            print(error.info())
            print(json.loads(error.read()))
            return error.info()

if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)
