$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/csv/bold-app.csv",
        dataType: "text",
        success: function(data) {processAppNames(data);}
     });
 });

 function showRecommender() {
    $('#slide-wrap').hide('slide', {direction: 'left'}, 1000);
    $('#slide-wrap2').show('slide', {direction: 'right'}, 1000);

    $("#nav-home").removeClass("active");
    $("#nav-recommender").addClass("active");
}

function processAppNames(allText) {
    var recordNum = 8;
    var allTextLines = allText.split(/\r\n|\n/)
    var appNames = [];

    for(var j = 1; j < allTextLines.length; j++) {
        appNames.push(allTextLines[j].split(',')[6]);
    }

    appNames.sort();

    $.each(appNames, function() {
        $("[id^='app-']").append($("<option />").val(this).text(this));
    })
}

var $form = $("#recommender-form");

function validateForm() {
    var a = [];
    $("#recommender-form select").each(function() {
        a.push($(this).val());
    });

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
    var unique = a.filter( onlyUnique );
    if(unique.length != a.length) {
        $("#validation-modal").modal();
        return false;
    } else {
        $('#slide-wrap2').hide('slide', {direction: 'left'}, 1000);
        $('#slide-wrap3').show('slide', {direction: 'right'}, 1000);
    }
}

var count = 0;
var cycleArray = ["a single app on your phone?", "your location?", "your income?"];
setInterval(function() {
    count++;
    $("#jumbotron-text-cycle").slideUp(400, function() {
        $(this).text(cycleArray[count % cycleArray.length]).slideDown(400);
    });
}, 3000);
